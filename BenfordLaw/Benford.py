a = [1, 1]
b = int(input("length: "))
for i in a:
    if len(a) < b:
        a.append(a[-1] + a[-2])
    else:
        break
		
one_count = 0
two_count = 0
three_count = 0
four_count = 0
five_count = 0
six_count = 0
seven_count = 0
eight_count = 0
nine_count = 0
for ix in range(len(a)):
    first_no = str(a[ix])
    if (first_no[0] == '1'):
        one_count += 1
    elif (first_no[0] == '2'):
        two_count += 1
    elif (first_no[0] == '3'):
        three_count += 1
    elif (first_no[0] == '4'):
        four_count += 1
    elif (first_no[0] == '5'):
        five_count += 1
    elif (first_no[0] == '6'):
        six_count += 1
    elif (first_no[0] == '7'):
        seven_count += 1
    elif (first_no[0] == '8'):
        eight_count += 1
    elif (first_no[0] == '9'):
        nine_count += 1

print("1: " + str(one_count) + " " + str((one_count/b) * 100) + "%")
print("2: " + str(two_count) + " " + str((two_count/b) * 100) + "%")
print("3: " + str(three_count) + " " + str((three_count/b) * 100) + "%")
print("4: " + str(four_count) + " " + str((four_count/b) * 100) + "%")
print("5: " + str(five_count) + " " + str((five_count/b) * 100) + "%")
print("6: " + str(six_count) + " " + str((six_count/b) * 100) + "%")
print("7: " + str(seven_count) + " " + str((seven_count/b) * 100) + "%")
print("8: " + str(eight_count) + " " + str((eight_count/b) * 100) + "%")
print("9: " + str(nine_count) + " " + str((nine_count/b) * 100) + "%")

import numpy as np
import matplotlib.pyplot as plt
height = [one_count/b * 100, two_count/b * 100, three_count/b * 100, four_count/b * 100, five_count/b * 100, six_count/b * 100, seven_count/b * 100, eight_count/b * 100, nine_count/b * 100]
bars = ('1', 'B', 'C', 'D', 'E', '6', '7', '8', '9')
y_pos = np.arange(len(bars))
plt.bar(y_pos, height, color=(0.2, 0.4, 0.6, 0.6))